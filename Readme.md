# Gabriel Ochoa
##### 3 Decatur St Apt 3, Brooklyn, NY 11216 | (512) 826-5402 |  gabeochoa@gmail.com | [gabeochoa.com](https://gabeochoa.com)

### Squarespace 
##### Senior Site Reliability Engineer (SRE) / Team Lead | New York, NY | August 2017 - Present 
```
- Led a team of 7 engineers in launching, getting organization buy-in, and migrating 
  3000 VMs owned by 15 teams to a new internal compute platform. The new platform replaced 
  the software runtime, metrics, alerting, and CI/CD layers of the Squarespace stack
- Created team charter and led agile development processes for the SRE Compute team
- Created and led Kubernetes 101 and 102 training courses for 100+ engineers
- SRE Lead for launch of a new billing system; started SRE Partner program
- Participated in Team and Incident Commander 24/7 on-call rotations
```
 
### Wink
##### Systems Engineer | Quirky Inc | New York, NY | August 2014 - December 2015 
```
- Technical product expert for integration of Amazon Echo, Nest, Schlage Locks, and many 
  ZigBee and Z-Wave products into the Wink platform.  
- Researched new and emerging smart home technologies (LoRoWan, BLE Beacons, etc)
- Led vendors and manufacturers in Asia through final stages of development and testing of 
  ZigBee, Z-Wave, and Bluetooth embedded devices
- Trained and helped launch a customer service center of 50+ employees
- Designed hardware and processes to refurbish 40,000 company products in 1 month
```

##### Site Reliability Engineer | Flextronics | New York, NY | December 2015 - August 2017 

```
- Created AWS-hosted Kubernetes clusters and migrated all applications from Heroku
- Participated in Design Thinking workshops and user story sessions
- Built internal product for creating micro-service infrastructure by developers
- Was on-call for and supported 18 micro-services and underlying infrastructure
- Designed and built a fault-tolerant logging and metrics pipeline
- Developed and championed SRE culture/practices across engineering organization
```

### Quirky 
##### BlackOps Design Engineer | New York, NY | January 2014 - August 2014 
```
- Prototyping specialist for CEO’s personal four-member design team
- Demonstrated products to executives, retail buyers, and investors
- Constructed visual and functional prototypes for Quirky Aros and Wink Relay
```

### Frog Design 
##### Design Technologist Intern | Austin, TX May 2013 - August 2013 


<br><br>
<br><br>

## Skills

#### Software

Kubernetes | Prometheus | Containers | AWS | GCP | Linux | Go | Python | Ruby | Redis | Etcd | MySQL | 
Memcached |
DynamoDB | CircleCI | Datadog | SumoLogic | Ansible 

#### Hardware

CNC/Manual Machining and Prototyping | Addative Manufacturing | Welding | Soldering | Wood/Metalworking | Solidworks |
AutoCAD/Fusion 360 | HSMWorks | Pro-E/Creo

## Education

### Georgia Institute of Technology | B.S. Mechanical Engineering 2013 
##### Manufacturing Processes | Machine Design | Prototyping | Product Design

-----

https://github.com/gabe-ochoa/resume