# Resume

My resume rendered from markdown. 

Archive folder holds every generated resume file over the years.

resume.md and resume.pdf are the current resume


## Usage

You'll need chrome and grip (github markdown generation) installed.


```
pip install grip
```

npm install -S htmlto

To generate pdf from resume.md:

```
make
```